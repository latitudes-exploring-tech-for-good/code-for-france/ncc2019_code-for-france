# Code for France : Transparence électorale

+ *Thème :* accès à l'information.
+ *Phase d'avancement actuelle :* en production.
+ *Compétences associés :* pas de compétence particulière nécessaire.

##### #0 | Résultats obtenus lors de la Nuit du Code Citoyen
Avant la NCC : Députés/sénateurs a priori ok, maires : 700 renseignés pour ~35000 communes.
Après la NCC : + 900 maires sur Wikidata, associés à leurs communes + quelques visualisations !
[Plus d'infos](https://frama.link/cff-wikidata).

##### #1 | Présentation de Code for France
Nous développons des idées et inventons des outils au service d'un monde numérique libre et ouvert offrant des opportunités à tou‧te‧s les citoyen‧ne‧s.

##### #2 | Problématique
Quelle parité dans les conseils municipaux ? Quelle est la moyenne d’âge à l’Assemblée nationale ? Quelle est la profession la plus pratiquée par nos élu‧e‧s ?
Voici quelques questions auxquelles nous pourrons répondre... quand nous aurons des données plus exhaustives !

##### #3 | Le défi proposé
Mettons nos élu‧e‧s sur Wikidata ! En soutien au projet [EveryPolitician](https://www.mysociety.org/democracy/everypolitician/) de mySociety, [Code for France](https://codefor.fr/) vous propose de renforcer la transparence électorale en découvrant et en contribuant à Wikidata. Ce véritable bien commun numérique servira à enrichir les informations dont nous disposons sur nos élu‧e‧s, afin de mieux connaître notre démocratie.

##### #4 | Livrables
Objectif : Ajout sur Wikidata de toutes les données du Répertoire national des élus sur les maires des communes de plus de 50 000 habitant en cours de mandat.

##### #5 | Ressources à disposition pour résoudre le défi
* [Notre pad](https://pad.codefor.fr/s/HkFnkGp5X).
* Le [Répertoire national des élus](https://www.data.gouv.fr/fr/datasets/5c34c4d1634f4173183a64f1/), qui vient tout juste d’être publié en open data.
* [Wikidata](http://wikidata.org/), la base de données structurées de Wikipédia.
* Des outils comme [Mix'n'match](https://tools.wmflabs.org/mix-n-match/), [Quickstatements](https://tools.wmflabs.org/quickstatements/), [OpenRefine](http://openrefine.org/)...
* Un maximum de contributeurs ! qui seront accompagné pour découvrir et utiliser les ressources (vous verrez, c'est très facile)

##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de peer-learning ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ Le transfert de méthodes et postures, qui pourront être appliquées et transmises aux équipes par les animateur.trice.s, les mentors ET les autres équipes ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Johan : membre de Code for France et porteur de projet.